import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    //TODO: add event listeners for all fighter elements
    this.getFighters().then((data) => {  
      console.log(data[id - 1].name); 
      console.log(data[id - 1]._id);
      console.log(data[id - 1].source);
      console.log('\n');
    })
    
  }
}

export const fighterService = new FighterService();
